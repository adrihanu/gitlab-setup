# GitLab setup

Setup for `GitLab` container.

Tags: `GitLab`, `Docker`, `Docker Compose`

## Running

`docker-compose up -d`

## Clear Mattermost channel messages

Find `id` in `About`: `6xpuubh58j8w8q14k1adgynsio`

```
sudo docker-compose exec web gitlab-psql -d mattermost_production
DELETE FROM Posts WHERE Posts.ChannelId = '6xpuubh58j8w8q14k1adgynsio'
```
